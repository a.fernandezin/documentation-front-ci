﻿---
categories:
- Projects
title: "Proyecto de ejemplo POC"
description: "Este es un proyecto de ejemplo"
date: 2022-02-18T13:45:05+01:00
draft: false
typeProjects: 
 - POC-Prueba de concepto
typeFronts:
 - Angular  
tags:
 - Modulos Federados  
 - Ngrx
gitLink: "https://gitlab.atmira.com/nexum/sirec/cex/sirec-micro-frontend/angular-mf-poc-4"
---
## Esta es una POC que implementa modulos federados y tambien usa el motor de estados de Ngrx.

﻿---
categories:
- Projects
title: "angular-mf-poc-1"
description: "Carga de módulo de angular expuesto como modulo federado."
date: 2022-02-18T13:45:05+01:00
draft: false
typeProjects: 
 - POC-Prueba de concepto
typeFronts:
 - Angular  
tags:
 - Modulos Federados   
 - Angular 13
gitLink: "https://gitlab.atmira.com/nexum/sirec/cex/sirec-micro-frontend/angular-mf-poc-1"
---
# angular-mf-poc-1

**Descripcion:**

POC prueba carga de módulo de angular expuesto como modulo federado.

## Caracteristicas

[ ] con angular 13.1.

[ ] MIcro front con module Federation

[ ] Un contenedor con un projectos federados

[ ] Utiliza la funcion de await loadRemoteModule() de federation para importa el modulo.


## Comentarios

como usa loadRemoteModule :

```
{
    path: 'mfe1',
    loadChildren: () => loadRemoteModule({
      type: 'module',
      remoteEntry: 'http://localhost:5000/remoteEntry.js',
      exposedModule: './Module'
    }).then(m => m.ModuleOneModule)
  }

```

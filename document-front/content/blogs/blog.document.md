﻿---
categories: [Blogs]
title: Documentación en Hugo
description: Este es un blog que explica comos se documenta en Hugo
date: 2022-02-18T12:45:05.000Z
draft: false
---
![picture 4](/images/ae0b572dbc96db16c7088b68727713c1660d4d922a75d69d39a27093edaeb682.png)

- [1. Documentación Hugo:](#1-documentación-hugo)
  - [1.1. Objetivos](#11-objetivos)
  - [1.2. Hugo](#12-hugo)
    - [1.2.1. Estructura de Hugo](#121-estructura-de-hugo)
    - [1.2.2. FrontMatter](#122-frontmatter)
  - [1.3. Contenidos de Front](#13-contenidos-de-front)
    - [1.3.1. Contenidos de Proyecto](#131-contenidos-de-proyecto)
    - [1.3.2. Contenidos de blog](#132-contenidos-de-blog)
    - [1.3.3. Todas las categorias disponibles:](#133-todas-las-categorias-disponibles)

# 1. Documentación Hugo:

## 1.1. Objetivos

Nuestro objetivo es documentar todos los proyectos y documentos que redactemos sobre Front, de una forma sencilla y directa. tambien podamos acceder y buscar por los tipos de contenido.

## 1.2. Hugo

Con Hugo podremos documentar de una forma sencilla y directa , estas son las características fundamentales:

generador páginas staticas.

- sistema de documentos markdown
- utilización de layouts y parciales para generar la pagina.
- velocidad en generar las paginas.
- sistema de taxonomia para clasificar la documentación y estructurarla.
- utilización de lunr para buscar contenidos.

[Enlace a Hugo Documentación](https://gohugo.io/documentation/)

### 1.2.1. Estructura de Hugo

Tendremos esta estructura de carpetas en este caso a partir de la carpeta de Hugo que hemos generado, 'document-front':

![](/assets/20220223_091650_image.png)

* content, para la carpeta de los contenidos
* layouts, para la carpeta de los layouts y parciales
* static, para los staticos de la web como son las imagenes , js y css
* themes, para los themes de la web
* public, en esta carpeta se almenacena la web ya compilada y procesada
* data, para almacenar los datos de la web puede ser en formato JSON,YAML..

### 1.2.2. FrontMatter

Es una metainformación que se pone al principio del contenido. Usaremos el frontmatter para la meta información del contenido de esta forma Hugo sabra como tratar dicho contenido. Tambien se usara el frontmatter par el buscador de contenidos.
Ejemplo:

```
---
title: "Blogs de Front"
date: 2022-02-18T13:45:05+01:00
draft: false
----
...Contenido
```

---

## 1.3. Contenidos de Front

Los contenidos de Front estarán en la carpeta Contents, los clasificaremos en dos secciones principales :

- blogs, serán blogs de Front
- projects, haran referencia alos proyectos de Front

### 1.3.1. Contenidos de Proyecto

Los añadimos en la carpeta /content/projects.

Ejemplo de proyecto:

```
---
categories:
- Projects
title: "Proyecto de ejemplo POC"
description: "Este es un proyecto de ejemplo"
date: 2022-02-18T13:45:05+01:00
draft: false
typeProjects: 
 - POC-Prueba de concepto
typeFronts:
 - Angular  
tags:
 - Modulos Federados  
 - Ngrx
gitLink: "https://gitlab.atmira.com/nexum/sirec/cex/sirec-micro-frontend/angular-mf-poc-4"
---
## Esta es una POC que implementa modulos federados y tambien usa el motor de estados de Ngrx.

```

Los campos del frontmatter son:

- **categories** , para taxonomia, en este caso es Projects
- **title**, titulo de nuestro proyecto
- **description**, descripción del proyecto
- **draft**, campo si es borrador
- **typeProjects**, para taxonomia, puede ser 'POC-Prueba de concepto','Arquetipo','Libreria','Herramienta'
- **TypeFronts**, para taxonomia, puede ser 'Angular','React'
- **tags**, para taxonomia, puede ser cualquier tag ....
- **gitLink**, enlance al Git donde está el proyecto,

Despues expondremos el contenido

### 1.3.2. Contenidos de blog

Los añadimos en la carpeta /content/projects.

Ejemplo de proyecto:

```
---
categories:
- Blogs
title: "Blog de de ejemplo"
description: "Este es un blog de ejemplo, para Angular"
date: 2022-02-18T13:45:05+01:00
draft: false
typeFront:
 - Angular
---
# Blog de ejemplo de Angular:
```

Los campos del frontmatter son:

- **categories** , para taxonomia, en este caso es Blogs
- **title**, titulo de nuestro proyecto
- **description**, descripción del proyecto
- **draft**, campo si es borrador
- **TypeFronts**, para taxonomia, puede ser 'Angular','React'
- **tags**, para taxonomia, puede ser cualquier tag ....

Despues expondremos el contenido

### 1.3.3. Todas las categorias disponibles:

{{<categories>}}
